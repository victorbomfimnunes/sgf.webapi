﻿using SGF.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;

namespace SGF.WebApi.Controllers
{
    public class PontoEletronicoController : ApiController
    {
        public ICollection<Registro> GetByID(Guid id)
        {
            using (var contexto = new DBRepository())
            {
                var ponto = contexto.PontoEletronico.FirstOrDefault();

                return ponto.Registros.Where(x => x.Guid == id).ToList();
            }
        }

        public void Post(Registro registro)
        {
            PontoEletronico pontoEletronico = null;

            using (var db = new DBRepository())
            {
                pontoEletronico = db.PontoEletronico.FirstOrDefault();

                if (pontoEletronico != null)
                {
                    pontoEletronico.RegistrarPonto(registro);
                    db.Entry(pontoEletronico).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    pontoEletronico = new PontoEletronico
                    {
                        Registros = new List<Registro> { registro }
                    };
                    db.PontoEletronico.Add(pontoEletronico);
                    db.SaveChanges();
                }
            }
        }
    }
}
