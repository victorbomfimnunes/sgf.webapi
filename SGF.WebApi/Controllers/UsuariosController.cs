﻿using SGF.WebApi.Models;
using SGF.WebApi.Models.Requests;
using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;

namespace SGF.WebApi.Controllers
{
    public class UsuariosController : ApiController
    {
        public IHttpActionResult Get()
        {
            using (var db = new DBRepository())
            {
                return Ok(db.Usuarios.ToList());
            }
        }

        public IHttpActionResult Get(Guid id)
        {
            using (var db = new DBRepository())
            {
                var usuario = db.Usuarios.FirstOrDefault(x => x.Guid == id);

                if (usuario !=null)
                {
                    return Ok(usuario);
                }
                else
                {
                    return NotFound();
                }
            }
        }

        public IHttpActionResult Post(UsuarioRequest usuarioRequest)
        {
            var usuario = Usuario.FactoryIncluir(usuarioRequest.Login, usuarioRequest.SenhaCripto, usuarioRequest.Email,
                usuarioRequest.Permissao, usuarioRequest.Ativo);

            using (var db = new DBRepository())
            {
                db.Usuarios.Add(usuario);
                db.SaveChanges();
            }

            return Ok();
        }

        public IHttpActionResult Put(UsuarioRequest usuarioRequest)
        {
            using (var db = new DBRepository())
            {
                var usuario = db.Usuarios.FirstOrDefault(x => x.Guid == usuarioRequest.Guid);
                usuario = Usuario.FactoryAlterar(usuarioRequest.SenhaCripto, usuarioRequest.Email,
                usuarioRequest.Permissao, usuarioRequest.Ativo);

                db.Entry(usuario).State = EntityState.Modified;
                db.SaveChanges();
            }

            return Ok();
        }

        public IHttpActionResult Delete(Guid id)
        {
            using (var db = new DBRepository())
            {
                var usuario = db.Usuarios.FirstOrDefault(x => x.Guid == id);
                db.Usuarios.Remove(usuario);
                db.SaveChanges();

                return Ok();
            }
        }
    }
}
