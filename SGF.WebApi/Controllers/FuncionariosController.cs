﻿using SGF.WebApi.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace SGF.WebApi.Controllers
{
    public class FuncionariosController : ApiController
    {
        public IEnumerable<Funcionario> GetAll()
        {
            using (var contexto = new DBRepository())
            {
                return contexto.Funcionarios.ToList();

            }
        }

        public IHttpActionResult GetById(int id)
        {
            Funcionario funcionario = null;

            using (var contexto = new DBRepository())
            {
                funcionario = contexto.Funcionarios.FirstOrDefault(x => x.Id == id);
            }

            if (funcionario != null)
            {
                return Ok(funcionario);
            }
            else
            {
                return NotFound();
            }
        }

        public IHttpActionResult Post(Funcionario funcionario)
        {
            using (var contexto = new DBRepository())
            {
                contexto.Funcionarios.Add(funcionario);
                contexto.SaveChanges();
            }

            return Created(funcionario.Id.ToString(), funcionario);
        }

        [Route("{id:int}/ferias")]
        [HttpGet]
        public IEnumerable<Ferias> Ferias(int id)
        {
            using (var contexto = new DBRepository())
            {
                var ferias = contexto.Ferias.Where(x => x.FuncionarioId == id).ToList();
                return ferias;
            }
        }

        [Route("{id:int}/ferias")]
        [HttpPost]
        public void Ferias(int id, Ferias ferias)
        {
            using (var contexto = new DBRepository())
            {
                contexto.Ferias.Add(new Ferias
                {
                    FuncionarioId = id,
                    Periodo = ferias.Periodo
                });
                contexto.SaveChanges();
            }
        }


        //[Route("{id:guid}/pontoeletronico")]
        //[HttpDelete]
        //public IHttpActionResult DeletePontoEletronico(Guid id)
        //{
        //    using (var db = new DBRepository())
        //    {
        //        var usuario = db.PontoEletronico.FirstOrDefault(x => x.UserGuid == id);
        //        db.PontoEletronico.Remove(usuario);
        //        db.SaveChanges();

        //        return Ok();
        //    }
        //}
    }
}
