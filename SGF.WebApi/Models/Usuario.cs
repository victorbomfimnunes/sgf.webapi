﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SGF.WebApi.Models
{
    public class Usuario
    {
        [Key]
        public Guid Guid { get; set; }
        public string Login { get; set; }
        public string SenhaCripto { get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime? UltimaAlteracao { get; set; }
        public string Email { get; set; }
        public PermissaoUsuario Permissao { get; set; }
        public bool Ativo { get; set; }

        public Usuario() { }

        private Usuario(Guid Guid, string Login, string SenhaCripto, DateTime DataCriacao, DateTime? UltimaAlteracao, string Email,
            PermissaoUsuario Permissao, bool Ativo)
        {
            this.Guid = Guid;
            this.Login = Login;
            this.SenhaCripto = SenhaCripto;
            this.DataCriacao = DataCriacao;
            this.UltimaAlteracao = UltimaAlteracao;
            this.Email = Email;
            this.Permissao = Permissao;
            this.Ativo = Ativo;
        }

        private Usuario(string SenhaCripto, DateTime UltimaAlteracao, string Email,
            PermissaoUsuario Permissao, bool Ativo)
        {
            this.SenhaCripto = SenhaCripto;
            this.UltimaAlteracao = UltimaAlteracao;
            this.Email = Email;
            this.Permissao = Permissao;
            this.Ativo = Ativo;
        }

        public static Usuario FactoryIncluir(string login, string senhaCripto, string email, PermissaoUsuario permissao, bool ativo)
        {
            return new Usuario(Guid.NewGuid(), login, senhaCripto, DateTime.Now, null, email, permissao, ativo);
        }

        internal static Usuario FactoryAlterar(string senhaEncrypt, string email, PermissaoUsuario permissao, bool ativo)
        {
            return new Usuario(senhaEncrypt, DateTime.Now, email, permissao, ativo);
        }
    }
}