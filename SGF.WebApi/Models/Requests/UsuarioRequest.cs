﻿using System;

namespace SGF.WebApi.Models.Requests
{
    public class UsuarioRequest
    {
        public Guid Guid { get; set; }
        public string Login { get; set; }
        public string SenhaCripto { get; set; }
        public string Email { get; set; }
        public PermissaoUsuario Permissao { get; set; }
        public bool Ativo { get; set; }
    }
}