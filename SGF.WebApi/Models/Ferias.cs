﻿namespace SGF.WebApi.Models
{
    public class Ferias
    {
        public int Id { get; set; }
        public int FuncionarioId { get; set; }
        public virtual Periodo Periodo{ get; set; }
    }
}