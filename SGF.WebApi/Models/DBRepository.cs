﻿using System.Data.Entity;

namespace SGF.WebApi.Models
{
    public class DBRepository : DbContext
    {
        public DbSet<Funcionario> Funcionarios { get; set; }
        public DbSet<Ferias> Ferias { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<PontoEletronico> PontoEletronico { get; set; }
    }
}