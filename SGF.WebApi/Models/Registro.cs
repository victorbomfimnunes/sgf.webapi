﻿using System;

namespace SGF.WebApi.Models
{
    public class Registro
    {
        public int RegistroID { get; set; }
        public Guid Guid { get; set; }
        public DateTime Horario { get; set; }
    }
}