﻿namespace SGF.WebApi.Models
{
    public enum PermissaoUsuario
    {
        ADMINISTRADOR,
        FUNCIONARIO
    }
}