﻿namespace SGF.WebApi.Models
{
    public class Horario
    {
        public int Id { get; set; }
        public int Hora { get; set; }
        public int Minuto { get; set; }

        public override string ToString()
        {
            return $"{Hora}:{Minuto}";
        }
    }
}