﻿using System.Collections.Generic;

namespace SGF.WebApi.Models
{
    public class JornadaTrabalho
    {
        public int Id { get; set; }

        public virtual ICollection<Jornada> Jornadas { get; set; }

        public int QuantidadeHoras { get; set; }

        public TipoContrato TipoContrato { get; set; }
    }
}