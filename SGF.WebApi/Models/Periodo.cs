﻿using System;

namespace SGF.WebApi.Models
{
    public class Periodo
    {
        public DateTime Inicio { get; set; }
        public DateTime Fim { get; set; }
    }
}