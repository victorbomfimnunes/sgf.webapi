﻿using System;

namespace SGF.WebApi.Models
{
    public class Jornada
    {
        public int Id { get; set; }
        public TimeSpan Entrada { get; set; }
        public TimeSpan Saida { get; set; }
    }
}