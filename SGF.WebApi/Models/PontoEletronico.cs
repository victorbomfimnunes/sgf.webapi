﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SGF.WebApi.Models
{
    public class PontoEletronico
    {
        public int PontoEletronicoID { get; set; }
        public virtual ICollection<Registro> Registros { get; set; }

        public PontoEletronico()
        {
            Registros = new List<Registro>();
        }

        public void RegistrarPonto(Registro novoRegistro)
        {
            Registros.Add(novoRegistro);
        }
    }
}