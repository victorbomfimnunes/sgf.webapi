﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using SGF.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace SGF.WebApi
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            try
            {
                using (var db = new DBRepository())
                {
                    var senhaCripto = db.Usuarios.FirstOrDefault(x => x.Login == context.UserName);

                    var usuario = db.Usuarios.FirstOrDefault(x => x.Login == context.UserName && x.SenhaCripto.ToLower() == context.Password.ToLower());

                    if (usuario == null)
                    {
                        context.SetError("invalid_grant", "Usuário ou senha inválidos");
                        return;
                    }
                    else
                    {
                        var oAuthIdentity = new ClaimsIdentity(context.Options.AuthenticationType);
                        oAuthIdentity.AddClaim(new Claim(ClaimTypes.Name, usuario.Login));
                        oAuthIdentity.AddClaim(new Claim(ClaimTypes.Email, usuario.Email));
                        oAuthIdentity.AddClaim(new Claim(ClaimTypes.Role, usuario.Permissao.ToString()));

                        var roles = oAuthIdentity.Claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var properties = CreateProperties(usuario.Login, roles.Select(x => x.Value).First(),
                            usuario.Guid);

                        var ticket = new AuthenticationTicket(oAuthIdentity, properties);
                        context.Validated(ticket);
                        context.Request.Context.Authentication.SignIn(oAuthIdentity);


                        //var properties = CreateProperties(usuario.Login,
                        //    Newtonsoft.Json.JsonConvert.SerializeObject(roles.Select(x => x.Value)));

                        //var ticket = new AuthenticationTicket(oAuthIdentity, properties);
                        //context.Validated(ticket);
                        //context.Request.Context.Authentication.SignIn(oAuthIdentity);
                    }
                }
            }
            catch(Exception e)
            {
                context.SetError("invalid_grant", "Falha ao autenticar");
            }


        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateProperties(string userName, string roles, Guid guid)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName },
                { "roles", roles},
                { "guid", guid.ToString()}
            };
            return new AuthenticationProperties(data);
        }
    }
}