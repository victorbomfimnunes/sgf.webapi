namespace SGF.WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Registroes", "Guid", c => c.Guid(nullable: false));
            DropColumn("dbo.Registroes", "Gui");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Registroes", "Gui", c => c.Guid(nullable: false));
            DropColumn("dbo.Registroes", "Guid");
        }
    }
}
