namespace SGF.WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Registroes", "PontoEletronico_UserGuid", "dbo.PontoEletronicoes");
            DropIndex("dbo.Registroes", new[] { "PontoEletronico_UserGuid" });
            RenameColumn(table: "dbo.Registroes", name: "PontoEletronico_UserGuid", newName: "PontoEletronico_Id");
            DropPrimaryKey("dbo.PontoEletronicoes");
            AddColumn("dbo.PontoEletronicoes", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Registroes", "PontoEletronico_Id", c => c.Int());
            AddPrimaryKey("dbo.PontoEletronicoes", "Id");
            CreateIndex("dbo.Registroes", "PontoEletronico_Id");
            AddForeignKey("dbo.Registroes", "PontoEletronico_Id", "dbo.PontoEletronicoes", "Id");
            DropColumn("dbo.PontoEletronicoes", "UserGuid");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PontoEletronicoes", "UserGuid", c => c.Guid(nullable: false));
            DropForeignKey("dbo.Registroes", "PontoEletronico_Id", "dbo.PontoEletronicoes");
            DropIndex("dbo.Registroes", new[] { "PontoEletronico_Id" });
            DropPrimaryKey("dbo.PontoEletronicoes");
            AlterColumn("dbo.Registroes", "PontoEletronico_Id", c => c.Guid());
            DropColumn("dbo.PontoEletronicoes", "Id");
            AddPrimaryKey("dbo.PontoEletronicoes", "UserGuid");
            RenameColumn(table: "dbo.Registroes", name: "PontoEletronico_Id", newName: "PontoEletronico_UserGuid");
            CreateIndex("dbo.Registroes", "PontoEletronico_UserGuid");
            AddForeignKey("dbo.Registroes", "PontoEletronico_UserGuid", "dbo.PontoEletronicoes", "UserGuid");
        }
    }
}
