namespace SGF.WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Jornadas", "JornadaTrabalho_Id", "dbo.JornadaTrabalhoes");
            DropForeignKey("dbo.Funcionarios", "JornadaTrabalho_Id", "dbo.JornadaTrabalhoes");
            DropIndex("dbo.Funcionarios", new[] { "JornadaTrabalho_Id" });
            DropIndex("dbo.Jornadas", new[] { "JornadaTrabalho_Id" });
            DropColumn("dbo.Funcionarios", "JornadaTrabalho_Id");
            DropTable("dbo.JornadaTrabalhoes");
            DropTable("dbo.Jornadas");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Jornadas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Entrada = c.Time(nullable: false, precision: 7),
                        Saida = c.Time(nullable: false, precision: 7),
                        JornadaTrabalho_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.JornadaTrabalhoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        QuantidadeHoras = c.Int(nullable: false),
                        TipoContrato = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Funcionarios", "JornadaTrabalho_Id", c => c.Int());
            CreateIndex("dbo.Jornadas", "JornadaTrabalho_Id");
            CreateIndex("dbo.Funcionarios", "JornadaTrabalho_Id");
            AddForeignKey("dbo.Funcionarios", "JornadaTrabalho_Id", "dbo.JornadaTrabalhoes", "Id");
            AddForeignKey("dbo.Jornadas", "JornadaTrabalho_Id", "dbo.JornadaTrabalhoes", "Id");
        }
    }
}
