namespace SGF.WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Ferias",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FuncionarioId = c.Int(nullable: false),
                        Periodo_Inicio = c.DateTime(nullable: false),
                        Periodo_Fim = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Funcionarios",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                        Cargo = c.String(),
                        AreaAtuacao = c.String(),
                        DataAdmissao = c.DateTime(nullable: false),
                        JornadaTrabalho_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.JornadaTrabalhoes", t => t.JornadaTrabalho_Id)
                .Index(t => t.JornadaTrabalho_Id);
            
            CreateTable(
                "dbo.JornadaTrabalhoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        QuantidadeHoras = c.Int(nullable: false),
                        TipoContrato = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Jornadas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Entrada = c.Time(nullable: false, precision: 7),
                        Saida = c.Time(nullable: false, precision: 7),
                        JornadaTrabalho_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.JornadaTrabalhoes", t => t.JornadaTrabalho_Id)
                .Index(t => t.JornadaTrabalho_Id);
            
            CreateTable(
                "dbo.PontoEletronicoes",
                c => new
                    {
                        PontoEletronicoID = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.PontoEletronicoID);
            
            CreateTable(
                "dbo.Registroes",
                c => new
                    {
                        RegistroID = c.Int(nullable: false, identity: true),
                        Gui = c.Guid(nullable: false),
                        Horario = c.DateTime(nullable: false),
                        PontoEletronico_PontoEletronicoID = c.Int(),
                    })
                .PrimaryKey(t => t.RegistroID)
                .ForeignKey("dbo.PontoEletronicoes", t => t.PontoEletronico_PontoEletronicoID)
                .Index(t => t.PontoEletronico_PontoEletronicoID);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        Guid = c.Guid(nullable: false),
                        Login = c.String(),
                        SenhaCripto = c.String(),
                        DataCriacao = c.DateTime(nullable: false),
                        UltimaAlteracao = c.DateTime(),
                        Email = c.String(),
                        Permissao = c.Int(nullable: false),
                        Ativo = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Guid);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Registroes", "PontoEletronico_PontoEletronicoID", "dbo.PontoEletronicoes");
            DropForeignKey("dbo.Funcionarios", "JornadaTrabalho_Id", "dbo.JornadaTrabalhoes");
            DropForeignKey("dbo.Jornadas", "JornadaTrabalho_Id", "dbo.JornadaTrabalhoes");
            DropIndex("dbo.Registroes", new[] { "PontoEletronico_PontoEletronicoID" });
            DropIndex("dbo.Jornadas", new[] { "JornadaTrabalho_Id" });
            DropIndex("dbo.Funcionarios", new[] { "JornadaTrabalho_Id" });
            DropTable("dbo.Usuarios");
            DropTable("dbo.Registroes");
            DropTable("dbo.PontoEletronicoes");
            DropTable("dbo.Jornadas");
            DropTable("dbo.JornadaTrabalhoes");
            DropTable("dbo.Funcionarios");
            DropTable("dbo.Ferias");
        }
    }
}
