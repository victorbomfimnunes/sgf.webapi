namespace SGF.WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Registroes", "PontoEletronico_Id", "dbo.PontoEletronicoes");
            DropIndex("dbo.Registroes", new[] { "PontoEletronico_Id" });
            RenameColumn(table: "dbo.Registroes", name: "PontoEletronico_Id", newName: "PontoEletronico_UserGuid");
            DropPrimaryKey("dbo.PontoEletronicoes");
            AddColumn("dbo.PontoEletronicoes", "UserGuid", c => c.Guid(nullable: false));
            AlterColumn("dbo.Registroes", "PontoEletronico_UserGuid", c => c.Guid());
            AddPrimaryKey("dbo.PontoEletronicoes", "UserGuid");
            CreateIndex("dbo.Registroes", "PontoEletronico_UserGuid");
            AddForeignKey("dbo.Registroes", "PontoEletronico_UserGuid", "dbo.PontoEletronicoes", "UserGuid");
            DropColumn("dbo.PontoEletronicoes", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PontoEletronicoes", "Id", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.Registroes", "PontoEletronico_UserGuid", "dbo.PontoEletronicoes");
            DropIndex("dbo.Registroes", new[] { "PontoEletronico_UserGuid" });
            DropPrimaryKey("dbo.PontoEletronicoes");
            AlterColumn("dbo.Registroes", "PontoEletronico_UserGuid", c => c.Int());
            DropColumn("dbo.PontoEletronicoes", "UserGuid");
            AddPrimaryKey("dbo.PontoEletronicoes", "Id");
            RenameColumn(table: "dbo.Registroes", name: "PontoEletronico_UserGuid", newName: "PontoEletronico_Id");
            CreateIndex("dbo.Registroes", "PontoEletronico_Id");
            AddForeignKey("dbo.Registroes", "PontoEletronico_Id", "dbo.PontoEletronicoes", "Id");
        }
    }
}
