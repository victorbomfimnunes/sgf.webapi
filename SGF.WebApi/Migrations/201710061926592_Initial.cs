namespace SGF.WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Registroes", "PontoEletronico_Id", "dbo.PontoEletronicoes");
            DropIndex("dbo.Registroes", new[] { "PontoEletronico_Id" });
            RenameColumn(table: "dbo.Registroes", name: "PontoEletronico_Id", newName: "PontoEletronicoId");
            DropPrimaryKey("dbo.PontoEletronicoes");
            DropPrimaryKey("dbo.Registroes");
            AddColumn("dbo.PontoEletronicoes", "PontoEletronicoID", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.Registroes", "RegistroID", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.Registroes", "Gui", c => c.Guid(nullable: false));
            AlterColumn("dbo.Registroes", "PontoEletronicoId", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.PontoEletronicoes", "PontoEletronicoID");
            AddPrimaryKey("dbo.Registroes", "RegistroID");
            CreateIndex("dbo.Registroes", "PontoEletronicoId");
            AddForeignKey("dbo.Registroes", "PontoEletronicoId", "dbo.PontoEletronicoes", "PontoEletronicoID", cascadeDelete: true);
            DropColumn("dbo.PontoEletronicoes", "Id");
            DropColumn("dbo.Registroes", "Id");
            DropColumn("dbo.Registroes", "Guid");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Registroes", "Guid", c => c.Guid(nullable: false));
            AddColumn("dbo.Registroes", "Id", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.PontoEletronicoes", "Id", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.Registroes", "PontoEletronicoId", "dbo.PontoEletronicoes");
            DropIndex("dbo.Registroes", new[] { "PontoEletronicoId" });
            DropPrimaryKey("dbo.Registroes");
            DropPrimaryKey("dbo.PontoEletronicoes");
            AlterColumn("dbo.Registroes", "PontoEletronicoId", c => c.Int());
            DropColumn("dbo.Registroes", "Gui");
            DropColumn("dbo.Registroes", "RegistroID");
            DropColumn("dbo.PontoEletronicoes", "PontoEletronicoID");
            AddPrimaryKey("dbo.Registroes", "Id");
            AddPrimaryKey("dbo.PontoEletronicoes", "Id");
            RenameColumn(table: "dbo.Registroes", name: "PontoEletronicoId", newName: "PontoEletronico_Id");
            CreateIndex("dbo.Registroes", "PontoEletronico_Id");
            AddForeignKey("dbo.Registroes", "PontoEletronico_Id", "dbo.PontoEletronicoes", "Id");
        }
    }
}
